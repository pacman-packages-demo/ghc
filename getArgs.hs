module Main where
    import Prelude ((++), (!!), IO, putStrLn)
    import System.Environment (getArgs)

    main :: IO ()
    main = do
        args <- getArgs
        putStrLn ("Hello, " ++ args !! 0)

-- from: https://en.wikibooks.org/wiki/Write_Yourself_a_Scheme_in_48_Hours/First_Steps

